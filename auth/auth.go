package auth

import (
	"fmt"
)

type Cred struct {
	User string
}

type Auth struct {
	DB     map[string]*Cred
	Locked bool
}

func (a *Auth) AddCredentials(user, token string) error {
	if a.Locked {
		return fmt.Errorf("Auth DB is locked")
	}

	if _, exists := a.DB[token]; exists {
		return fmt.Errorf("Token already exists in Auth DB: %s", token)
	}

	a.DB[token] = &Cred{
		User: user,
	}

	return nil
}

func (a *Auth) Check(token string) (*Cred, error) {
	c, ok := a.DB[token]
	if !ok {
		return nil, fmt.Errorf("Invalid credentials")
	}

	return c, nil
}

func (a *Auth) Lock() {
	a.Locked = true
}

func New() *Auth {
	a := &Auth{}

	a.DB = map[string]*Cred{}
	a.Locked = false

	return a
}
