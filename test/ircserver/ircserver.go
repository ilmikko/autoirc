// Mock IRC server for integration tests.
package ircserver

import (
	"fmt"

	"gitlab.com/ilmikko/autoirc/test"
	"gitlab.com/ilmikko/autoirc/test/ircserver/tcpserver"
)

type IRCServer struct {
	Host      string
	Port      string
	TCPServer *tcpserver.TCPServer
	users     []string
}

func (is *IRCServer) Close() {
	is.TCPServer.Close()
}

func (is *IRCServer) Users() []string {
	return is.users
}

func (is *IRCServer) Flush() {
	is.TCPServer.Flush()
}

func (is *IRCServer) Query(q string) (string, error) {
	return is.TCPServer.Query(q)
}

func New() (*IRCServer, error) {
	is := &IRCServer{
		Host:  "localhost",
		Port:  fmt.Sprintf("%v", test.RandomPort()),
		users: []string{},
	}

	var err error
	is.TCPServer, err = tcpserver.New(
		is.Host,
		is.Port,
	)
	if err != nil {
		return nil, fmt.Errorf("TCP server creation failed: %v", err)
	}

	is.initIRCRules()

	return is, nil
}
