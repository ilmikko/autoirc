package ircserver

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
)

func (is *IRCServer) writeLoginInfo(c net.Conn) {
	fmt.Fprintf(c, "%s", "irc.example.net 001 user-name :Welcome to the Internet Relay Network user-name!~user-name@test-host\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 002 user-name :Your host is irc.example.net, running version ircd-42 (x86_64/pc/linux-gnu)\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 003 user-name :This server has been started Tue Sep 11 2017 at 12:05:09 (UTC)\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 004 user-name irc.example.net ircd-42 aBcdDefgHiJkLmn abcdefghiJKLMnoPqRStuVz\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 005 user-name RFC2812 IRCD=ircd CHARSET=UTF-8 CASEMAPPING=ascii PREFIX=(abcde)~&@%+ CHANTYPES=#&+ CHANMODES=abc,d,e,fgHiJKLmnOPq CHANLIMIT=#&+:1000 :are supported on this server\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 005 user-name CHANNELLEN=999 NICKLEN=20 TOPICLEN=900 AWAYLEN=256 KICKLEN=256 MODES=100 MAXLIST=abC:10 EXCEPTS=a INVEX=I PENALTY :are supported on this server\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 251 user-name :There are 28 users and 7 services on 10 servers\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 254 user-name 49 :channels formed\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 255 user-name :I have 28 users, 7 services and 10 servers\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 265 user-name 28 1 :Current local users: 28, Max: 80\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 266 user-name 42 1 :Current global users: 28, Max: 80\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 250 user-name :Highest connection count: 20 (4800 connections received)\r\n")
	fmt.Fprintf(c, "%s", "irc.example.net 422 user-name :MOTD file is missing\r\n")
}

func (is *IRCServer) initIRCRules() {
	is.TCPServer.Handle(func(c net.Conn) error {
		r := bufio.NewReader(c)
		s, err := r.ReadString('\n')
		if err != nil {
			return err
		}

		fields := strings.Fields(s)

		switch fields[0] {
		case "NICK":
			s, err := r.ReadString('\n')
			if err != nil {
				return err
			}

			fields := strings.Fields(s)
			switch fields[0] {
			case "USER":
				log.Printf("Login: %v", fields)
				is.users = append(is.users, fields[1])
				is.writeLoginInfo(c)
			}
		}

		log.Printf("CLIENT: %v", fields)
		fmt.Fprintf(c, "HELLO WORLD")
		return nil
	})
}
