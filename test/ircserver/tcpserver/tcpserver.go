package tcpserver

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"time"
)

const (
	READ_TIMEOUT  = 1 * time.Second
	FLUSH_TIMEOUT = 10 * time.Millisecond
)

type TCPServer struct {
	Host       string
	Port       string
	Connection net.Conn
	handler    func(net.Conn) error
	listener   net.Listener
}

func (ts *TCPServer) handleConn(c net.Conn) error {
	ts.Connection = c
	return ts.handler(c)
}

func (ts *TCPServer) Close() {
	ts.listener.Close()
}

func (ts *TCPServer) Flush() {
	if ts.Connection == nil {
		log.Printf("Trying to flush nonexistent connection")
		return
	}

	for {
		_, err := ts.Read(FLUSH_TIMEOUT)
		if err, ok := err.(net.Error); ok && err.Timeout() {
			break
		}
		if err != nil {
			log.Printf("Flush error: %v", err)
			break
		}
	}
}

func (ts *TCPServer) Read(timeout time.Duration) (string, error) {
	if ts.Connection == nil {
		return "", fmt.Errorf("No active connection on TCP server!")
	}

	ts.Connection.SetReadDeadline(time.Now().Add(
		timeout,
	))
	return bufio.NewReader(ts.Connection).ReadString('\n')
}

func (ts *TCPServer) Write(s string) error {
	if ts.Connection == nil {
		return fmt.Errorf("No active connection on TCP server!")
	}

	s = fmt.Sprintf("%s\r\n", s)

	log.Printf("WRITING: [%s]", s)
	fmt.Fprintf(ts.Connection, "%s", s)
	return nil
}

func (ts *TCPServer) Query(q string) (string, error) {
	ts.Flush()
	if err := ts.Write(q); err != nil {
		return "", fmt.Errorf("Writer error: %v", err)
	}

	s, err := ts.Read(READ_TIMEOUT)
	if err != nil {
		return "", fmt.Errorf("Reader error: %v", err)
	}

	return s, nil
}

func (ts *TCPServer) Handle(h func(net.Conn) error) {
	ts.handler = h
}

func (ts *TCPServer) Listen() {
	for {
		conn, err := ts.listener.Accept()
		if err != nil {
			log.Printf("listener.Accept failed: %v", err)
			break
		}
		if err := ts.handleConn(conn); err != nil {
			log.Printf("handle failed: %v", err)
		}
	}
}

func New(host, port string) (*TCPServer, error) {
	ls, err := net.Listen(
		"tcp",
		fmt.Sprintf("%s:%v",
			host,
			port,
		),
	)
	if err != nil {
		return nil, fmt.Errorf("net.Listen(%s, :%v) failed: %v", "tcp",
			port,
			err,
		)
	}

	ts := &TCPServer{
		Host:     host,
		Port:     port,
		listener: ls,
	}
	go ts.Listen()

	return ts, nil
}
