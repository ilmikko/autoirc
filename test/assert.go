package test

import (
	"fmt"
)

func AssertString(want, got string) error {
	if want == got {
		return nil
	}

	return fmt.Errorf(
		"Strings are not equal:\nwant: %s\n got: %s",
		want,
		got,
	)
}
