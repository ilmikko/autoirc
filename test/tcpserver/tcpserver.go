package tcpserver

import (
	"bufio"
	"fmt"
	"net"
	"time"

	"gitlab.com/ilmikko/autoirc/test"
)

const (
	TIMEOUT = 1 * time.Second
)

type TCPServer struct {
	Host     string
	Port     string
	listener net.Listener
}

func (ts *TCPServer) Close() {
	if ts.listener == nil {
		return
	}
	ts.listener.Close()
}

type TCPServerReader struct {
	Conn   net.Conn
	reader *bufio.Reader
}

func (r *TCPServerReader) Read() (string, error) {
	r.Conn.SetReadDeadline(time.Now().Add(
		TIMEOUT,
	))
	s, err := r.reader.ReadString('\n')
	if err != nil {
		return "", err
	}

	return s, nil
}

func (ts *TCPServer) Reader() (*TCPServerReader, error) {
	c, err := ts.listener.Accept()
	if err != nil {
		return nil, err
	}

	r := bufio.NewReader(c)
	return &TCPServerReader{
		Conn:   c,
		reader: r,
	}, nil
}

type TCPServerWriter struct {
	Conn net.Conn
}

func (w *TCPServerWriter) Write(s string) error {
	fmt.Fprintf(w.Conn, "%s", s)
	return nil
}

func (ts *TCPServer) Writer() (*TCPServerWriter, error) {
	c, err := ts.listener.Accept()
	if err != nil {
		return nil, err
	}

	return &TCPServerWriter{
		Conn: c,
	}, nil
}

func (ts *TCPServer) Listen() error {
	var err error
	ts.listener, err = net.Listen("tcp",
		fmt.Sprintf("%s:%s",
			ts.Host,
			ts.Port,
		),
	)
	if err != nil {
		return fmt.Errorf("Cannot create server: %v", err)
	}
	return nil
}

func New() (*TCPServer, error) {
	ts := &TCPServer{
		Host: "localhost",
		Port: fmt.Sprintf("%v",
			test.RandomPort(),
		),
	}

	if err := ts.Listen(); err != nil {
		return nil, err
	}

	return ts, nil
}
