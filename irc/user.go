package irc

import (
	"gitlab.com/ilmikko/autoirc/irc/client"
)

type User struct {
	Name     string
	Channels []string
	Client   *client.Client
}

func (u *User) Join(channel string) error {
	if err := u.Client.Join(channel); err != nil {
		return err
	}
	u.Channels = append(u.Channels, channel)
	return nil
}

func (u *User) Close() error {
	if err := u.Client.Close(); err != nil {
		return err
	}
	return nil
}

func (u *User) Quit(msg string) error {
	if err := u.Client.Quit(msg); err != nil {
		return err
	}
	return nil
}

func (u *User) Send(channel, user string) error {
	if err := u.Client.Send(channel, user); err != nil {
		return err
	}
	return nil
}
