package irc_test

import (
	"reflect"
	"testing"

	"gitlab.com/ilmikko/autoirc/config"
	"gitlab.com/ilmikko/autoirc/irc"
	"gitlab.com/ilmikko/autoirc/test/ircserver"
)

func TestConnectionRegister(t *testing.T) {
	is, err := ircserver.New()
	if err != nil {
		t.Fatalf("Cannot initialize test IRC server: %v", err)
	}
	defer is.Close()

	cfg := config.New()
	cfg.Host = is.Host
	cfg.Port.IRC = is.Port

	conn, err := irc.NewConnection(cfg)
	if err != nil {
		t.Fatalf("NewConnection: %v", err)
	}

	{
		_, err := conn.User("test-user")
		if err != nil {
			t.Errorf("Cannot create test-user: %v", err)
		}

		want := []string{"test-user"}
		got := is.Users()
		if !reflect.DeepEqual(want, got) {
			t.Errorf("User list not as expected:\nwant: %v\n got: %v",
				want,
				got,
			)
		}
	}

	{
		_, err := conn.User("test-user-2")
		if err != nil {
			t.Errorf("Cannot create test-user-2: %v", err)
		}

		want := []string{"test-user", "test-user-2"}
		got := is.Users()
		if !reflect.DeepEqual(want, got) {
			t.Errorf("User list not as expected:\nwant: %v\n got: %v",
				want,
				got,
			)
		}
	}
}

func TestPingPong(t *testing.T) {
	is, err := ircserver.New()
	if err != nil {
		t.Fatalf("Cannot initialize test IRC server: %v", err)
	}
	defer is.Close()

	cfg := config.New()
	cfg.Host = is.Host
	cfg.Port.IRC = is.Port

	conn, err := irc.NewConnection(cfg)
	if err != nil {
		t.Fatalf("NewConnection: %v", err)
	}

	{
		_, err := conn.User("ping-user")
		if err != nil {
			t.Errorf("Cannot create ping-user: %v", err)
		}

		query := "PING :test-server"
		got, err := is.Query(query)
		if err != nil {
			t.Errorf("is.Query(%s) failed: %v", query, err)
		}
		want := "PONG ping-user\r\n"
		if got != want {
			t.Errorf("PING response invalid:\nwant: %s\n got: %s",
				want,
				got,
			)
		}
	}
}
