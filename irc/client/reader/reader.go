package reader

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"strings"
	"time"

	"gitlab.com/ilmikko/autoirc/irc/client/status"
)

const (
	READ_TIMEOUT  = 1 * time.Minute
	FAST_TIMEOUT  = 1 * time.Second
	FLUSH_TIMEOUT = 10 * time.Millisecond
)

type IRCReader struct {
	conn   net.Conn
	reader *bufio.Reader
}

func (r *IRCReader) Flush() ([]*status.Status, error) {
	ss := []*status.Status{}

	for {
		sta := r.read(FLUSH_TIMEOUT)

		if sta.IsTimeout() || sta.IsEOF() {
			break
		}

		if sta.IsError() {
			return ss, fmt.Errorf("Flush: %s", sta.Message)
		}

		ss = append(ss, sta)
	}

	return ss, nil
}

func (r *IRCReader) Read() *status.Status {
	return r.read(READ_TIMEOUT)
}

func (r *IRCReader) ReadFast() *status.Status {
	return r.read(FAST_TIMEOUT)
}

func (r *IRCReader) ReadResponse() (string, error) {
	st := r.Read()
	if st.IsError() {
		return "", fmt.Errorf("ReadFast: %s", st.Message)
	}

	return st.Message, nil
}

func (r *IRCReader) read(timeout time.Duration) *status.Status {
	r.conn.SetReadDeadline(time.Now().Add(
		timeout,
	))

	line, err := r.reader.ReadString('\n')

	// TODO: Change this functionality to status.Ping, status.Timeout etc.
	// These can then have more respective fields, like the host that is pinging in status.Ping
	if err != nil {
		if err, ok := err.(net.Error); ok && err.Timeout() {
			return &status.Status{
				Code:    status.STA_TIMEOUT,
				Message: fmt.Sprintf("Timeout read: %v", err),
			}
		}
		if err == io.EOF {
			return &status.Status{
				Code:    status.STA_EOF,
				Message: fmt.Sprintf("EOF read: %v", err),
			}
		}
		// Not sure how to do this better?
		if strings.HasSuffix(err.Error(), "use of closed network connection") {
			// Reading while connection closed counts as EOF for us.
			return &status.Status{
				Code:    status.STA_EOF,
				Message: fmt.Sprintf("Closed read: %v", err),
			}
		}
		return &status.Status{
			Code:    status.STA_ERROR,
			Message: fmt.Sprintf("Read error: %v", err),
		}
	}

	words := strings.Split(line, " ")

	if words[0] == "PING" {
		return &status.Status{
			Code:    status.STA_PING,
			Message: strings.Join(words, " "),
		}
	}

	s := &status.Status{}
	s.Code = words[1]
	s.Message = strings.Join(words, " ")

	return s
}

func NewReader(conn net.Conn) *IRCReader {
	return &IRCReader{
		conn:   conn,
		reader: bufio.NewReader(conn),
	}
}
