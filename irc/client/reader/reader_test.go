package reader_test

import (
	"fmt"
	"net"
	"reflect"
	"testing"

	"gitlab.com/ilmikko/autoirc/irc/client/reader"
	"gitlab.com/ilmikko/autoirc/irc/client/status"
	"gitlab.com/ilmikko/autoirc/test/tcpserver"
)

func TestPing(t *testing.T) {
	ts, err := tcpserver.New()
	if err != nil {
		t.Fatalf("Cannot create TCP server: %v", err)
	}
	defer ts.Close()

	c, err := net.Dial("tcp",
		fmt.Sprintf("%s:%s",
			ts.Host,
			ts.Port,
		),
	)
	if err != nil {
		t.Fatalf("Cannot create connection: %v", err)
	}

	r := reader.NewReader(c)
	w, err := ts.Writer()
	if err != nil {
		t.Fatalf("Cannot initialize writer: %v", err)
	}

	w.Write("PING irc.example.net\r\n")

	got := r.Read()
	want := &status.Status{
		Code:    "900",
		Message: "PING irc.example.net\r\n",
	}

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Ping result not as expected:\nwant: %v\n got: %v",
			want,
			got,
		)
	}
}

func TestResponseRead(t *testing.T) {
	ts, err := tcpserver.New()
	if err != nil {
		t.Fatalf("Cannot create TCP server: %v", err)
	}
	defer ts.Close()

	c, err := net.Dial("tcp",
		fmt.Sprintf("%s:%s",
			ts.Host,
			ts.Port,
		),
	)
	if err != nil {
		t.Fatalf("Cannot create connection: %v", err)
	}

	r := reader.NewReader(c)
	w, err := ts.Writer()
	if err != nil {
		t.Fatalf("Cannot initialize writer: %v", err)
	}

	w.Write("irc.example.net 001 test-user :Welcome to the Internet Relay Network test-user!\r\n")
	w.Write("irc.example.net 002 test-user :Your host is irc.example.net, running version ircd-42 (x86_64/pc/linux-gnu)\r\n")

	{
		got := r.Read()
		want := &status.Status{
			Code:    "001",
			Message: "irc.example.net 001 test-user :Welcome to the Internet Relay Network test-user!\r\n",
		}
		if !reflect.DeepEqual(want, got) {
			t.Errorf("Read result not as expected:\nwant: %v\n got: %v",
				want,
				got,
			)
		}
	}

	{
		got := r.Read()
		want := &status.Status{
			Code:    "002",
			Message: "irc.example.net 002 test-user :Your host is irc.example.net, running version ircd-42 (x86_64/pc/linux-gnu)\r\n",
		}
		if !reflect.DeepEqual(want, got) {
			t.Errorf("Read result not as expected:\nwant: %v\n got: %v",
				want,
				got,
			)
		}
	}
}

func TestFlush(t *testing.T) {
	ts, err := tcpserver.New()
	if err != nil {
		t.Fatalf("Cannot create TCP server: %v", err)
	}
	defer ts.Close()

	c, err := net.Dial("tcp",
		fmt.Sprintf("%s:%s",
			ts.Host,
			ts.Port,
		),
	)
	if err != nil {
		t.Fatalf("Cannot create connection: %v", err)
	}

	r := reader.NewReader(c)
	w, err := ts.Writer()
	if err != nil {
		t.Fatalf("Cannot initialize writer: %v", err)
	}

	// Fill the write buffer
	for i := 0; i < 10; i++ {
		w.Write("irc.example.net 005 test-user :Hello World!\r\n")
	}

	{
		got := r.Read()
		want := &status.Status{
			Code:    "005",
			Message: "irc.example.net 005 test-user :Hello World!\r\n",
		}
		if !reflect.DeepEqual(want, got) {
			t.Errorf("Ping result not as expected:\nwant: %v\n got: %v",
				want,
				got,
			)
		}
	}

	r.Flush()
	w.Write("irc.example.net 001 test-user :Welcome to the Internet Relay Network test-user!\r\n")

	{
		got := r.Read()
		want := &status.Status{
			Code:    "001",
			Message: "irc.example.net 001 test-user :Welcome to the Internet Relay Network test-user!\r\n",
		}
		if !reflect.DeepEqual(want, got) {
			t.Errorf("Ping result not as expected:\nwant: %v\n got: %v",
				want,
				got,
			)
		}
	}
}
