package status_test

import (
	"testing"

	"."
)

func TestSTA(t *testing.T) {
	{
		s := status.Status{
			Code:    status.STA_EOF,
			Message: "EOF",
		}

		if !s.IsEOF() {
			t.Errorf("status.STA_EOF returns false for s.IsEOF()")
		}
	}

	{
		s := status.Status{
			Code:    status.STA_TIMEOUT,
			Message: "Timeout",
		}

		if !s.IsTimeout() {
			t.Errorf("status.STA_TIMEOUT returns false for s.IsTimeout()")
		}
	}

	{
		s := status.Status{
			Code:    status.STA_PING,
			Message: "Ping!",
		}

		if !s.IsPING() {
			t.Errorf("status.STA_PING returns false for s.IsPING()")
		}
	}
}

func TestError(t *testing.T) {
	testCases := []struct {
		code string
		want bool
	}{
		// Anything with a code starting 0,2 or 3 should not be errors.
		{
			code: "001",
			want: false,
		},
		{
			code: "205",
			want: false,
		},
		{
			code: "313",
			want: false,
		},
		// Codes starting with 4 or 5 are usually errors...
		{
			code: "401",
			want: true,
		},
		{
			code: "405",
			want: true,
		},
		{
			code: "502",
			want: true,
		},
		// STA codes
		{
			code: status.STA_PING,
			want: false,
		},
		{
			code: status.STA_EOF,
			want: true,
		},
		{
			code: status.STA_TIMEOUT,
			want: true,
		},
		{
			code: status.STA_ERROR,
			want: true,
		},
		// Special "errors" that aren't really treated as errors in AutoIRC.
		{
			code: status.ERR_NOMOTD,
			want: false,
		},
		{
			code: status.ERR_NOADMININFO,
			want: false,
		},
	}

	for _, tc := range testCases {
		s := status.Status{
			Code: tc.code,
		}

		got := s.IsError()
		want := tc.want
		if got != want {
			t.Errorf("IsError for code %v not as expected:\nwant: %v\n got: %v",
				tc.code,
				want,
				got,
			)
		}
	}
}
