package status

import (
	"log"
	"regexp"
)

const (
	ERR_NOSUCHNICK        = "401"
	ERR_NOSUCHSERVER      = "402"
	ERR_NOSUCHCHANNEL     = "403"
	ERR_CANNOTSENDTOCHAN  = "404"
	ERR_TOOMANYCHANNELS   = "405"
	ERR_WASNOSUCHNICK     = "406"
	ERR_TOOMANYTARGETS    = "407"
	ERR_NOORIGIN          = "409"
	ERR_NORECIPIENT       = "411"
	ERR_NOTEXTTOSEND      = "412"
	ERR_NOTOPLEVEL        = "413"
	ERR_WILDTOPLEVEL      = "414"
	ERR_UNKNOWNCOMMAND    = "421"
	ERR_NOMOTD            = "422"
	ERR_NOADMININFO       = "423"
	ERR_FILEERROR         = "424"
	ERR_NONICKNAMEGIVEN   = "431"
	ERR_ERRONEUSNICKNAME  = "432"
	ERR_NICKNAMEINUSE     = "433"
	ERR_NICKCOLLISION     = "436"
	ERR_USERNOTINCHANNEL  = "441"
	ERR_NOTONCHANNEL      = "442"
	ERR_USERONCHANNEL     = "443"
	ERR_NOLOGIN           = "444"
	ERR_SUMMONDISABLED    = "445"
	ERR_USERSDISABLED     = "446"
	ERR_NOTREGISTERED     = "451"
	ERR_NEEDMOREPARAMS    = "461"
	ERR_ALREADYREGISTERED = "462"
	ERR_NOPERMFORHOST     = "463"
	ERR_PASSWDMISMATCH    = "464"
	ERR_YOUREBANNEDCREEP  = "465"
	ERR_KEYSET            = "467"
	ERR_CHANNELISFULL     = "471"
	ERR_UNKNOWNMODE       = "472"
	ERR_INVITEONLYCHAN    = "473"
	ERR_BANNEDFROMCHAN    = "474"
	ERR_BADCHANNELKEY     = "475"
	ERR_NOPRIVILEGES      = "481"
	ERR_CHANOPRIVSNEEDED  = "482"
	ERR_CANTKILLSERVER    = "483"
	ERR_NOOPERHOST        = "491"
	ERR_UMODEUNKNOWNFLAG  = "501"
	ERR_USERSDONTMATCH    = "502"
	STA_PING              = "900"
	STA_EOF               = "901"
	STA_TIMEOUT           = "902"
	STA_ERROR             = "999"
	STA_PRIVMSG           = "PRIVMSG"
)

var (
	responseGoodRx = regexp.MustCompile("^[023]\\d{1,2}$")
	responseBadRx  = regexp.MustCompile("^[459]\\d{1,2}$")
)

type Status struct {
	Code    string
	Message string
}

func (s *Status) IsEOF() bool {
	if s.Code == STA_EOF {
		return true
	}
	return false
}

func (s *Status) IsPING() bool {
	if s.Code == STA_PING {
		return true
	}
	return false
}

func (s *Status) IsTimeout() bool {
	if s.Code == STA_TIMEOUT {
		return true
	}
	return false
}

// Returns true only if this is a client error.
func (s *Status) IsError() bool {
	switch s.Code {
	case ERR_NOMOTD, ERR_NOADMININFO, STA_PING:
		return false
	case STA_ERROR, STA_TIMEOUT:
		return true
		// TODO: Refactor following to events
	case "JOIN":
		return false
	case "MODE":
		return false
	case "QUIT":
		return false
	case "PRIVMSG":
		return false
	}

	switch {
	case responseBadRx.MatchString(s.Code):
		return true
	case responseGoodRx.MatchString(s.Code):
		return false
	default:
		log.Printf("Unknown status: %q", s)
		return true
	}
}
