package writer_test

import (
	"fmt"
	"net"
	"testing"

	"gitlab.com/ilmikko/autoirc/irc/client/writer"
	"gitlab.com/ilmikko/autoirc/test/tcpserver"
)

func TestWriteCommands(t *testing.T) {
	ts, err := tcpserver.New()
	if err != nil {
		t.Fatalf("Cannot create TCP server: %v", err)
	}
	defer ts.Close()

	c, err := net.Dial("tcp",
		fmt.Sprintf("%s:%s",
			ts.Host,
			ts.Port,
		),
	)
	if err != nil {
		t.Fatalf("Cannot create connection: %v", err)
	}

	w := writer.NewWriter(c)
	r, err := ts.Reader()
	if err != nil {
		t.Fatalf("Cannot initialize reader: %v", err)
	}

	testCases := []struct {
		method func()
		want   string
	}{
		{
			method: func() {
				w.Join("chan")
			},
			want: "JOIN chan\r\n",
		},
		{
			method: func() {
				w.Nick("nickname")
			},
			want: "NICK nickname\r\n",
		},
		{
			method: func() {
				w.Pass("secret-password")
			},
			want: "PASS secret-password\r\n",
		},
		{
			method: func() {
				w.Pong("host")
			},
			want: "PONG host\r\n",
		},
		{
			method: func() {
				w.Privmsg("jane", "Hi Jane!")
			},
			want: "PRIVMSG jane :Hi Jane!\r\n",
		},
		{
			method: func() {
				w.Privmsg("#chan", "Hello World!")
			},
			want: "PRIVMSG #chan :Hello World!\r\n",
		},
		{
			method: func() {
				w.Quit("Leaving for a cuppa")
			},
			want: "QUIT :Leaving for a cuppa\r\n",
		},
		{
			method: func() {
				w.User("username", "host.name", "server.name", "Real Name")
			},
			want: "USER username host.name server.name :Real Name\r\n",
		},
	}

	for _, tc := range testCases {
		tc.method()

		got, err := r.Read()
		if err != nil {
			t.Fatalf("Reading from connection failed: %v", err)
		}

		want := tc.want
		if got != want {
			t.Errorf("Invalid socket write:\nwant: %s\n got: %s",
				want,
				got,
			)
		}
	}
}
