package writer

import (
	"fmt"
	"net"
)

type IRCWriter struct {
	conn net.Conn
}

func NewWriter(conn net.Conn) *IRCWriter {
	return &IRCWriter{
		conn: conn,
	}
}

// TODO: Change Fprintf to .Write([]byte)
func (c *IRCWriter) Quit(reason string) {
	fmt.Fprintf(c.conn,
		"%s",
		fmt.Sprintf("QUIT :%s\r\n", reason),
	)
}

func (c *IRCWriter) Pass(password string) {
	fmt.Fprintf(c.conn,
		"%s",
		fmt.Sprintf("PASS %s\r\n", password),
	)
}

func (c *IRCWriter) Nick(nickname string) string {
	return fmt.Sprintf("NICK %s\r\n", nickname)
}

func (c *IRCWriter) User(username, hostname, servername, realname string) string {
	return fmt.Sprintf("USER %s %s %s :%s\r\n",
		username,
		hostname,
		servername,
		realname,
	)
}

func (c *IRCWriter) Privmsg(recipients, msg string) {
	fmt.Fprintf(c.conn,
		"%s",
		fmt.Sprintf("PRIVMSG %s :%s\r\n",
			recipients,
			msg,
		),
	)
}

func (c *IRCWriter) Join(channel string) {
	fmt.Fprintf(c.conn,
		"%s",
		fmt.Sprintf("JOIN %s\r\n",
			channel,
		),
	)
}

func (c *IRCWriter) Pong(hostname string) error {
	fmt.Fprintf(c.conn,
		"%s",
		fmt.Sprintf("PONG %s\r\n", hostname),
	)
	return nil
}

func (c *IRCWriter) Write(s string) error {
	if _, err := c.conn.Write([]byte(s)); err != nil {
		return err
	}
	return nil
}
