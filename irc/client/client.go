package client

import (
	"fmt"
	"log"
	"net"
	"strings"

	"gitlab.com/ilmikko/autoirc/irc/client/reader"
	"gitlab.com/ilmikko/autoirc/irc/client/status"
	"gitlab.com/ilmikko/autoirc/irc/client/writer"
)

type Client struct {
	reader *reader.IRCReader
	writer *writer.IRCWriter

	conn net.Conn
	user string
}

func Connect(host, port string) (*Client, error) {
	conn, err := net.Dial("tcp",
		fmt.Sprintf("%s:%s", host, port),
	)
	if err != nil {
		return nil, fmt.Errorf("net.Dial(...) failed: %v", err)
	}

	c := NewClient(conn)
	return c, nil
}

func (c *Client) Close() error {
	if err := c.conn.Close(); err != nil {
		return err
	}
	return nil
}

func (c *Client) Idle() error {
	for {
		s := c.reader.Read()
		switch {
		case s.IsPING():
			c.writer.Pong(c.user)
		case s.IsTimeout():
			// Not interesting
		case s.IsEOF():
			log.Printf("EOF: %s", s.Message)
			return nil
		case s.Code == status.STA_PRIVMSG:
			fmt.Printf("%s", s.Message)
		case s.IsError():
			return fmt.Errorf("%s", s.Message)
		default:
			log.Printf("Ignored: %q", s)
		}
	}
}

func (c *Client) Join(channel string) error {
	c.writer.Join(
		fmt.Sprintf("%s", channel),
	)
	return nil
}

func (c *Client) Send(channel, msg string) error {
	c.writer.Privmsg(
		fmt.Sprintf("%s", channel),
		msg,
	)
	return nil
}

func (c *Client) Flush() ([]*status.Status, error) {
	ss, err := c.reader.Flush()
	if err != nil {
		return nil, err
	}

	// It's a good idea to send an extra PONG just in case we're flushing a PING.
	if err := c.Pong(); err != nil {
		return nil, err
	}
	return ss, nil
}

func (c *Client) Quit(msg string) error {
	c.writer.Quit(msg)
	if err := c.Close(); err != nil {
		return err
	}
	return nil
}

func (c *Client) Login(nick, pass string) error {
	c.user = nick
	if pass != "" {
		c.writer.Pass(pass)
	}

	c.Write(c.writer.Nick(nick))
	c.Write(c.writer.User(nick, "0", "*",
		fmt.Sprintf("AutoIRC Client (%s)", nick),
	))
	msg, err := c.Read()
	if err != nil {
		return err
	}
	log.Printf("Nick response: %q", msg)

	// Flush welcome messages.
	ws, err := c.Flush()
	if err != nil {
		return err
	}

	// TODO: Move to "parser"
	for _, w := range ws {
		switch w.Code {
		case "002":
			fields := strings.Fields(w.Message)
			log.Printf("Host: %s", fields[0])
		case "251":
			log.Printf("%s", w.Message)
		}
	}
	return nil
}

func (c *Client) Pong() error {
	if err := c.writer.Pong(c.user); err != nil {
		return err
	}
	return nil
}

func (c *Client) Read() (string, error) {
	msg, err := c.reader.ReadResponse()
	if err != nil {
		return "", err
	}
	return msg, nil
}

func (c *Client) WriteRead(msg string) (string, error) {
	if _, err := c.Flush(); err != nil {
		return "", err
	}

	if err := c.Write(msg); err != nil {
		return "", err
	}
	msg, err := c.Read()
	if err != nil {
		return "", err
	}

	return msg, nil
}

func (c *Client) Write(msg string) error {
	if err := c.writer.Write(msg); err != nil {
		return err
	}
	return nil
}

func NewClient(conn net.Conn) *Client {
	return &Client{
		conn:   conn,
		reader: reader.NewReader(conn),
		writer: writer.NewWriter(conn),
	}
}
