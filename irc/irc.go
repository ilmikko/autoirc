package irc

import (
	"gitlab.com/ilmikko/autoirc/config"
)

type IRC struct {
	CFG        *config.Config
	Connection *Connection
}

func (i *IRC) Quit(msg string) error {
	if err := i.Connection.Quit(msg); err != nil {
		return err
	}
	return nil
}

func (i *IRC) Close() error {
	if err := i.Connection.Close(); err != nil {
		return err
	}
	return nil
}

func (i *IRC) SendAll(msg string) error {
	for id, u := range i.Connection.Users {
		for _, c := range u.Channels {
			i.Send(id, c, msg)
		}
	}

	return nil
}

func (i *IRC) Send(id, ch, msg string) {
	i.Connection.Users[id].Send(ch, msg)
}

func New(cfg *config.Config) (*IRC, error) {
	i := &IRC{}

	c, err := NewConnection(cfg)
	if err != nil {
		return nil, err
	}

	i.CFG = cfg
	i.Connection = c

	return i, nil
}
