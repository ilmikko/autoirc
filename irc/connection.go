package irc

import (
	"fmt"
	"log"

	"gitlab.com/ilmikko/autoirc/config"
	"gitlab.com/ilmikko/autoirc/irc/client"
)

type Connection struct {
	CFG *config.Config

	Users map[string]*User
}

func (c *Connection) Close() error {
	for _, u := range c.Users {
		if err := u.Close(); err != nil {
			return err
		}
	}
	return nil
}

func (c *Connection) Quit(msg string) error {
	for _, u := range c.Users {
		if err := u.Quit(msg); err != nil {
			return err
		}
	}
	return nil
}

func (c *Connection) User(user string) (*User, error) {
	return c.UserPassword(user, "")
}

func (c *Connection) UserPassword(user, pass string) (*User, error) {
	cl, err := client.Connect(
		c.CFG.Host,
		c.CFG.Port.IRC,
	)
	if err != nil {
		return nil, fmt.Errorf("client.Connect(%s, %s) error: %v",
			c.CFG.Host,
			c.CFG.Port.IRC,
			err,
		)
	}

	if err := cl.Login(user, pass); err != nil {
		return nil, fmt.Errorf("Cannot login as user %q: %v", user, err)
	}

	go func() {
		if err := cl.Idle(); err != nil {
			// Fatal here as a read error is fatal for the connection.
			// We might want to try and reconnect in the future but currently that's extra logic.
			log.Fatalf("Idle error: %v", err)
		}
	}()

	return &User{
		Name:   user,
		Client: cl,
	}, nil
}

func NewConnection(cfg *config.Config) (*Connection, error) {
	c := &Connection{}

	c.CFG = cfg
	c.Users = map[string]*User{}

	for id, cu := range cfg.Users {
		u, err := c.UserPassword(id, "")
		if err != nil {
			return nil, err
		}

		c.Users[id] = u

		for _, c := range cu.Channels {
			u.Join(c)
		}
	}

	return c, nil
}
