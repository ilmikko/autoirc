package link

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

const (
	LINK_FILE = "/tmp/autoirc-link"
)

type Link struct {
	conn net.Conn
}

func (l *Link) Send(rcpt, msg string) {
	// TODO: This only works if msg does not contain newlines
	fmt.Fprintf(l.conn, "%s %s\n", rcpt, msg)
}

func (l *Link) auth(token string) {
	fmt.Fprintf(l.conn, "AUTH %s\n", token)
}

func Announce(port string) error {
	f, err := os.Create(LINK_FILE)
	if err != nil {
		return fmt.Errorf("Failed to create link file %s: %v", LINK_FILE, err)
	}
	defer f.Close()

	fmt.Fprintf(f, "%s", port)
	return nil
}

func getAddr() (string, error) {
	host := "localhost"

	f, err := os.Open(LINK_FILE)
	if err != nil {
		return "", fmt.Errorf("Failed to open link file %s: %v", LINK_FILE, err)
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	s.Scan()
	port := s.Text()

	return fmt.Sprintf("%s:%s",
		host,
		port,
	), nil
}

func Open(token string) (*Link, error) {
	addr, err := getAddr()
	if err != nil {
		return nil, err
	}

	conn, err := net.Dial("tcp",
		addr,
	)
	if err != nil {
		return nil, fmt.Errorf("net.Dial(tcp, %s) failed: %v", addr, err)
	}

	l := &Link{
		conn: conn,
	}

	l.auth(token)

	return l, nil
}
