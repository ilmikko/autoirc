package http

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

var (
	SERVE_MUX = http.DefaultServeMux
)

func Get(url url.URL) (string, error) {
	url.Scheme = "http"

	resp, err := http.Get(url.String())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func Register(path string, f func(http.ResponseWriter, *http.Request)) {
	SERVE_MUX.HandleFunc(fmt.Sprintf("/%s", path), f)
}

func RegisterArgs(path string, f func(args []string) string) {
	pathRegexp := regexp.MustCompile(
		fmt.Sprintf("\\/%s\\/(.+)", path),
	)

	SERVE_MUX.HandleFunc(
		fmt.Sprintf("/%s/", path),
		func(w http.ResponseWriter, r *http.Request) {
			matches := pathRegexp.FindStringSubmatch(r.URL.EscapedPath())

			if len(matches) < 2 {
				return
			}

			args := strings.Split(matches[1], "/")
			for i := range args {
				m, err := url.QueryUnescape(args[i])
				if err != nil {
					return
				}
				args[i] = m
			}

			fmt.Fprintf(w, f(args))
		},
	)
}
