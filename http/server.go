package http

import (
	"fmt"
	"log"
	"net/http"
)

func Listen(port string) {
	log.Printf("HTTP server listening on %s", port)
	Init()

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

func Init() {
	Register("", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Unknown request: %s", r.URL.Path)
	})
}
