package bridge

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"

	"gitlab.com/ilmikko/autoirc/auth"
	"gitlab.com/ilmikko/autoirc/config"
	"gitlab.com/ilmikko/autoirc/irc"
	"gitlab.com/ilmikko/autoirc/link"
)

type Bridge struct {
	CFG   *config.Config
	users map[string]*irc.User // TODO: Change to config.User
}

func (bg *Bridge) auth(r *bufio.Reader) (*auth.Cred, error) {
	s, err := r.ReadString('\n')
	if err != nil {
		return nil, err
	}

	fields := strings.Fields(s)
	if fields[0] != "AUTH" {
		return nil, fmt.Errorf("Malformed AUTH message")
	}
	if len(fields) < 2 {
		return nil, fmt.Errorf("No credentials provided")
	}

	cred, err := bg.CFG.Auth.Check(fields[1])
	if err != nil {
		return nil, fmt.Errorf("auth.Check failed: %v", err)
	}

	return cred, nil
}

func (bg *Bridge) serveSend(cred *auth.Cred, r *bufio.Reader) error {
	rcpt, err := r.ReadString(' ')
	if err != nil {
		return err
	}

	msg, err := r.ReadString('\n')
	if err != nil {
		return err
	}

	if strings.Contains(rcpt, "\n") {
		split := strings.SplitN(rcpt, "\n", 2)
		rcpt = split[0]
		msg = split[1]
	}

	msg = strings.TrimSuffix(
		strings.TrimSuffix(msg, "\n"),
		"\r",
	)

	bg.Send(cred, rcpt, msg)
	return nil
}

func (bg *Bridge) serveErr(conn net.Conn) error {
	r := bufio.NewReader(conn)

	cred, err := bg.auth(r)
	if err != nil {
		return fmt.Errorf("Unauthorized (%v)", err)
	}

	for {
		if err := bg.serveSend(cred, r); err != nil {
			return fmt.Errorf("Send error: %v", err)
		}
	}
	return nil
}

func (bg *Bridge) serve(conn net.Conn) {
	defer conn.Close()
	if err := bg.serveErr(conn); err != nil {
		log.Printf("Serve error: %v", err)
	}
}

func (bg *Bridge) Send(cred *auth.Cred, rcpt, msg string) {
	bg.users[cred.User].Send(rcpt, msg)
}

func (bg *Bridge) AddUser(user *irc.User) error {
	if _, ok := bg.users[user.Name]; ok {
		return fmt.Errorf("User with the same name already in map: %s", user.Name)
	}
	bg.users[user.Name] = user
	return nil
}

func (bg *Bridge) Listen() error {
	log.Printf("Bridge listening on %s:%s", bg.CFG.Host, bg.CFG.Port.Bridge)
	ln, err := net.Listen("tcp",
		fmt.Sprintf("%s:%s",
			bg.CFG.Host,
			bg.CFG.Port.Bridge,
		),
	)
	if err != nil {
		return fmt.Errorf("net.Listen failed: %v", err)
	}

	bg.StartLink()

	for {
		conn, err := ln.Accept()
		if err != nil {
			return fmt.Errorf("Failed to accept: %v", err)
		}
		go bg.serve(conn)
	}
	return nil
}

func (bg *Bridge) StartLink() {
	link.Announce(bg.CFG.Port.Bridge)
}

func New(cfg *config.Config) *Bridge {
	b := &Bridge{}
	b.CFG = cfg

	b.users = map[string]*irc.User{}

	return b
}
