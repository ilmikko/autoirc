package autoirc

import (
	"bufio"
	"io"
	"os"

	"gitlab.com/ilmikko/autoirc/bridge"
	"gitlab.com/ilmikko/autoirc/config"
	"gitlab.com/ilmikko/autoirc/irc"
)

type AutoIRC struct {
	CFG    *config.Config
	Bridge *bridge.Bridge
	IRC    *irc.IRC
}

func (airc *AutoIRC) Listen() error {
	airc.CFG.Auth.Lock()

	bridge := make(chan error)
	if airc.Bridge != nil {
		go func() {
			bridge <- airc.Bridge.Listen()
		}()
	}

	r := bufio.NewReader(os.Stdin)
	for {
		s, err := r.ReadString('\n')
		if err != nil {
			if err != io.EOF {
				return err
			}
			break
		}
		if err := airc.IRC.SendAll(s); err != nil {
			return err
		}
	}

	select {
	case err := <-bridge:
		return err
	default:
		airc.IRC.Quit("Done!")
		return nil
	}
}

func New(cfg *config.Config) (*AutoIRC, error) {
	airc := &AutoIRC{}

	airc.CFG = cfg

	{
		i, err := irc.New(cfg)
		if err != nil {
			return nil, err
		}
		airc.IRC = i
	}

	if cfg.Enable.Bridge {
		airc.Bridge = bridge.New(cfg)
	}

	return airc, nil
}
