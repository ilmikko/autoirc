package main

import (
	"log"

	"gitlab.com/ilmikko/autoirc/autoirc"
	"gitlab.com/ilmikko/autoirc/config"
)

func mainErr() error {
	if err := config.Init(); err != nil {
		return err
	}

	cfg, err := config.Load()
	if err != nil {
		return err
	}

	a, err := autoirc.New(cfg)
	if err != nil {
		return err
	}

	if err := a.Listen(); err != nil {
		return err
	}
	return nil
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("ERROR: %v", err)
	}
}
