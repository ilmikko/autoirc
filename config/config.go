package config

import (
	"fmt"

	"gitlab.com/ilmikko/autoirc/auth"
	"gitlab.com/ilmikko/autoirc/config/parser"
)

var (
	DEFAULT_ENABLED_BRIDGE = false
	DEFAULT_HOST           = "localhost"
	DEFAULT_PORT_BRIDGE    = "2222"
	DEFAULT_PORT_IRC       = "6667"
)

type Config struct {
	Auth   *auth.Auth
	Enable struct {
		Bridge bool
	}
	Host string
	Port struct {
		Bridge string
		IRC    string
	}
	Users map[string]*User

	parser *parser.Parser
}

func New() *Config {
	c := &Config{}

	c.Auth = auth.New()
	c.parser = parser.New()

	c.Enable.Bridge = DEFAULT_ENABLED_BRIDGE

	c.Host = DEFAULT_HOST

	c.Port.Bridge = DEFAULT_PORT_BRIDGE
	c.Port.IRC = DEFAULT_PORT_IRC

	c.Users = map[string]*User{}

	return c
}

func Load() (*Config, error) {
	c := New()
	if h := *flagHost; h != "" {
		c.Host = h
	}
	if p := *flagPortBridge; p != "" {
		c.Port.Bridge = p
	}
	if p := *flagPortIRC; p != "" {
		c.Port.IRC = p
	}
	if f := *flagConfigFile; f != "" {
		if err := c.Load(f); err != nil {
			return nil, fmt.Errorf("Load %q: %v", f, err)
		}

		for id, u := range c.Users {
			for _, tok := range u.Tokens {
				if err := c.Auth.AddCredentials(id, tok); err != nil {
					return nil, err
				}
			}
		}
	}
	return c, nil
}

func Init() error {
	flagParse()
	return nil
}
