package config

import "strings"

const (
	COMMENT_RUNE = '#'
)

func (cfg *Config) IsRule(line string) bool {
	fields := strings.Fields(line)

	// Empty line.
	if len(fields) == 0 {
		return false
	}

	// Comment.
	if fields[0][0] == COMMENT_RUNE {
		return false
	}

	return true
}

func (cfg *Config) ParseLine(line string, context ...string) error {
	fields := strings.Fields(line)

	if !cfg.IsRule(line) {
		return nil
	}

	// Additional context.
	if len(context) > 0 {
		fields = append(context, fields...)
	}

	return cfg.p(fields)
}
