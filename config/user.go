package config

import "fmt"

type User struct {
	ID       string
	Channels []string
	Tokens   []string
}

func (u *User) Join(channel string) error {
	u.Channels = append(u.Channels, channel)
	return nil
}

func (u *User) Token(tok string) error {
	u.Tokens = append(u.Tokens, tok)
	return nil
}

func (cfg *Config) User(id string) (*User, error) {
	u, ok := cfg.Users[id]
	if !ok {
		return nil, fmt.Errorf("Unknown user: %q", id)
	}
	return u, nil
}

func NewUser(id string) *User {
	u := &User{}

	u.ID = id
	u.Channels = []string{}
	u.Tokens = []string{}

	return u
}
