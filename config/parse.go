package config

import (
	"fmt"

	"gitlab.com/ilmikko/autoirc/config/parser"
)

func (cfg *Config) pConfig(fields []string) error {
	l := len(fields)
	file, context := fields[l-1], fields[:l-1]
	if err := cfg.Load(file, context...); err != nil {
		return fmt.Errorf("CONFIG %q: %v", fields, err)
	}
	return nil
}

func (cfg *Config) pUserCreate(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.Users[id] = NewUser(id)
	return nil
}

func (cfg *Config) pUserJoin(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	u, err := cfg.User(id)
	if err != nil {
		return err
	}

	channel, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	if err := u.Join(channel); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pUserToken(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	u, err := cfg.User(id)
	if err != nil {
		return err
	}

	tok, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	if err := u.Token(tok); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pUser(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CREATE": cfg.pUserCreate,
		"JOIN":   cfg.pUserJoin,
		"TOKEN":  cfg.pUserToken,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pIrcHost(fields []string) error {
	host, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.Host = host
	return nil
}

func (cfg *Config) pIrcPort(fields []string) error {
	port, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.Port.IRC = port
	return nil
}

func (cfg *Config) pIrc(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"PORT": cfg.pIrcPort,
		"HOST": cfg.pIrcHost,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) p(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CONFIG": cfg.pConfig,
		"IRC":    cfg.pIrc,
		"USER":   cfg.pUser,
	}); err != nil {
		return err
	}
	return nil
}
