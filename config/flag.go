package config

import "flag"

var (
	flagPortBridge = flag.String("http_port", "", "Port for the TCP bridge.")
	flagHost       = flag.String("host", "", "Host for bridge and IRC server.")
	flagPortIRC    = flag.String("irc_port", "", "Port for the local IRC server.")
	flagConfigFile = flag.String("config", "", "Configuration file.")
)

func flagParse() {
	flag.Parse()
}
