.PHONY: test

GITPATH=gitlab.com/ilmikko/autoirc

UNIT=./config/parser \
		 ./irc/client/writer \
		 ./irc/client/reader \
		 ./irc/client/status \
		 ./irc/client \
		 ./irc \

INTEGRATION=.
COVERFILE=coverage.out

build:
	mkdir -p /tmp/go/src/$(shell dirname $(GITPATH))
	test -L /tmp/go/src/$(GITPATH) || ln -sf $(shell pwd) /tmp/go/src/$(GITPATH)
	GOPATH="/tmp/go" go build -o bin/autoirc autoirc.go
	@rm -rf /tmp/go

test: test-unit test-integration

coverage: cover-unit cover-view

cover-unit:
	go test $(UNIT) -coverprofile=$(COVERFILE)

cover-view:
	go tool cover -html=$(COVERFILE)

test-unit:
	go test $(UNIT)

test-integration:
	go test $(INTEGRATION)
